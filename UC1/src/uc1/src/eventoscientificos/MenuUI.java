package eventoscientificos;

import java.io.IOException;
import utils.*;

/**
 *
 * @author Nuno Silva
 */
public class MenuUI
{
    private Empresa m_empresa;
    private String opcao;

    public MenuUI(Empresa empresa)
    {
        m_empresa = empresa;
    }

    public void run() throws IOException
    {
        do
        {
            //opcao = "1";

            System.out.println("1. Registar utilizador");
            System.out.println("2. Criar Evento");
            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");

            if( opcao.equals("1") )
            {
                RegistarUtilizadorUI uiRU = new RegistarUtilizadorUI(m_empresa);
                uiRU.run();
            }

        }
        while (!opcao.equals("0") );
    }
}
